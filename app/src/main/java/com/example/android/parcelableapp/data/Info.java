package com.example.android.parcelableapp.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DELL on 6/9/2015.
 */
public class Info implements Parcelable {

    String mId;
    String mValue;

    // constructor
    public Info(String id, String value){
        this.mId = id;
        this.mValue = value;
    }

    // parcelable
    protected Info(Parcel in){
        this.mId = in.readString();
        this.mValue = in.readString();
    }

    // getters
    public String getId() {
        return mId;
    }

    public String getValue() {
        return mValue;
    }

    // parcelable specific methods
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mValue);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Info> CREATOR = new Parcelable.Creator<Info>() {
        @Override
        public Info createFromParcel(Parcel in) {
            return new Info(in);
        }

        @Override
        public Info[] newArray(int size) {
            return new Info[size];
        }
    };
}
