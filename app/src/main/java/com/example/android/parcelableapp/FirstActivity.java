package com.example.android.parcelableapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.android.parcelableapp.data.Booking;
import com.example.android.parcelableapp.data.Info;

import java.util.ArrayList;
import java.util.List;


public class FirstActivity extends ActionBarActivity {

    private Button mBtnGo;
    private Booking mBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        initObjects();
        initGoButton();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // private helper methods //

    /**
     * Sets reference to 'go' button and sets onClick() listener.
     * The click listener launches SecondActivity with an intent, which will display the
     * contents of the created Booking object in initObjects()
     */
    private void initGoButton() {
        mBtnGo = (Button) findViewById(R.id.btn_go);

        mBtnGo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // works, need to pass Booking object to second activity
                Intent i = new Intent(getApplicationContext(), SecondActivity.class);
                i.putExtra("booking", mBooking);
                startActivity(i);
            }
        });
    }

    /**
     * Initializes the object booking with data and also creates
     * 3 member Info objects which are a part of Booking
     */
    private void initObjects() {
        List<Info> infos = new ArrayList<Info>() {
            {
                add(new Info("UXC12345", "first value"));
                add(new Info("TYK98765", "second value"));
                add(new Info("ABX23862", "third value"));
            }
        };

        mBooking = new Booking("6969AXU419420421", "Denis Johnson", 2, 4, 2015, infos);
    }
}
