package com.example.android.parcelableapp.data;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 6/9/2015.
 */
public class Booking implements Parcelable {

    private String mConfirmationNo;
    private String mName;
    private int mMonth;
    private int mDay;
    private int mYear;
    private List<Info> mInfos;

    // this should help with things: http://www.developerphil.com/parcelable-vs-serializable/

    // constructor
    public Booking(String confirmationNo, String name, int month, int day, int year, List<Info> infos){
        this.mConfirmationNo = confirmationNo;
        this.mMonth = month;
        this.mDay = day;
        this.mYear = year;
        this.mName = name;
        this.mInfos = new ArrayList<Info>(infos);
    }

    // parceling
    protected Booking(Parcel in) {
        this.mConfirmationNo = in.readString();
        this.mName = in.readString();

        this.mMonth = in.readInt();
        this.mDay = in.readInt();
        this.mYear = in.readInt();

        if(in.readByte() == 0x01) {
            mInfos = new ArrayList<Info>();
            in.readList(mInfos, Info.class.getClassLoader());
        } else {
            mInfos = null;
        }
    }

    // getters
    public String getConfirmationNo() {
        return mConfirmationNo;
    }

    public String getName() {
        return mName;
    }

    public int getMonth() {
        return mMonth;
    }

    public int getDay() {
        return mDay;
    }

    public int getYear() {
        return mYear;
    }

    public String getInfos(){
        String output = "Infos:";

        for(int i =0; i < mInfos.size(); i++)
             output += "\nID: " + mInfos.get(i).getId() + "\nValue: " + mInfos.get(i).getValue() + '\n';

        return output;
    }

    // parcelable specific methods
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mConfirmationNo);
        dest.writeString(mName);
        dest.writeInt(mMonth);
        dest.writeInt(mDay);
        dest.writeInt(mYear);
        if (mInfos == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mInfos);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Booking> CREATOR = new Parcelable.Creator<Booking>() {
        @Override
        public Booking createFromParcel(Parcel in) {
            return new Booking(in);
        }

        @Override
        public Booking[] newArray(int size) {
            return new Booking[size];
        }
    };
}
