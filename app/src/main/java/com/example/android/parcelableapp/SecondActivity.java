package com.example.android.parcelableapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.android.parcelableapp.data.Booking;


public class SecondActivity extends ActionBarActivity {

    // output identifiers
    private final String CONF_PREFIX = "Confirmation #: ";
    private final String DATE_PREFIX = "Date: ";
    private final String NAME_PREFIX = "Name: ";

    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mTextView = (TextView)findViewById(R.id.second_textview);
        displayObjectData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayObjectData(){
        Booking booking2 = getIntent().getParcelableExtra("booking");

        if(booking2 != null){
            mTextView.setText(
                            CONF_PREFIX + booking2.getConfirmationNo() + '\n' +
                            DATE_PREFIX + booking2.getMonth() + '/' + booking2.getDay() + '/' + booking2.getYear() + '\n' +
                            NAME_PREFIX + booking2.getName() + '\n' +
                            booking2.getInfos()
            );
        }
    }
}
